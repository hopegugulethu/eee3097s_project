# Shell script to automate the data capture, compression and encryption

for i in {1..10}
do
	python3 ICM20948.py > DataFiles/IMU_data_$i.txt
done
echo "10 data batches gathered"

for j in {1..10}
do
	python3 FFT.py DataFiles/IMU_data_$j.txt > FFTBefore/FFT_before_$j.txt
done
echo "FFT 1/2 complete"

for p in {1..10}
do
	python3 RunCompression.py DataFiles/IMU_data_$p.txt > C/C_$p/compressed_$p.txt
	mv treeFile.pkl C/C_$p
done
echo "Compression complete"

for q in {1..10}
do
	python3 RunEncryption.py C/C_$q/compressed_$q.txt key > E/encrypted_$q.txt
done
echo "Encryption complete"

for w in {1..10}
do	python3 RunDecryption.py E/encrypted_$w.txt key $w > Dx/decrypted_$w.txt
done
echo "Decryption complete"

for k in {1..10}
do
	python3 RunDecompression.py Dx/decrypted_$k.txt C/C_$k/treeFile.pkl > D/decompressed_$k.txt
done
echo "Decompression complete"

for l in {1..10}
do
	python3 FFT.py D/decompressed_$l.txt > FFTAfter/FFT_after_$l.txt
done
echo "FFT 2/2 complete"

echo "sytem operation complete :-D"
