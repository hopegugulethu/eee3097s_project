## EEE3097S: Designing Compression and Encryption subsysytems for IMU data

## Authors 
Hope G. Ngwenya and Msimamisi S. Mwandla

## Licence
This project is under the GNU GENERAL PUBLIC LICENSE

## Subsystem definition
### 1. Data Acquisition
		ICM20948.py
		
### 2. Data Compression
		RunCompression.py <original_file>
		
### 3. Data Encryption
		RunEncryption.py <compressed_file> <key>

### Running the system
#### This command runs the entire system including 1. Data Acquisition >> 2. Data Compression >> 3. Data Encryption

command: bash main.sh
