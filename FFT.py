# Read data and execute FFT
# Msimamisi Mwandla
# 20/10/2021

import sys
import numpy as np
from numpy.fft import fft

# iterate different batches of data gathered by IMU
for i in range(1):

    #filepath = "IMU_data_"+str(i)+".txt"
    filepath = sys.argv[1]
    
    print("[F{pitch}","F{roll}","F{x_Accel}","F{y_Accel}","F{z_Accel}","F_{x_AngVel}","F{y_AngVel}","F{z_AngVel}]")
    # open file and process each line
    with open(filepath) as f:

        lines = f.readlines()[1:]
        for line in lines:
           
            data = line.split(" ") # Enter data into an array

            readyData = []
            for j in range(len(data)):
                cleanData = data[j].strip() # Remove any whitespace
                print(cleanData)
                readyData.append(float(cleanData)) # Enter data into new array

            dataFFT= fft(readyData) # perform an FFT on the data
            MagDataFFT = np.abs(dataFFT) # Find the magnitude of frequency components
            
            output = ""
            for k in range(len(MagDataFFT)):
                if k ==0:
                    output += str(MagDataFFT[k])
                elif k > 0:
                    output += " "+str(MagDataFFT[k])
                
            print(output)



