import socket
import threading
import time
import TwoFishEncryptionDecryption
import compress

socket_object = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket_object.connect(("196.42.112.103", 1234))
data_array = []

message = socket_object.recv(1024)
data = message.decode("utf-8")
data_array.append(data)

encrypted_array = TwoFishEncryptionDecryption.encrypt_data(data)
compressed_data, tree = compress.compress(encrypted_array)

print("ORIGINAL DATA")
print(data_array)

print("ENCRYPTION")
print(encrypted_array)

print("COMPRESSION")
print(compressed_data)

print("DECOMPRESSION")
print("Decompressed: ", compress.decompress(compressed_data, tree))

print("DECRYPTION")
print(TwoFishEncryptionDecryption.prep_decrypt_data(TwoFishEncryptionDecryption.decrypt_data(encrypted_array)))




    




