# Msimamisi Mwandla
# 30/10/2021
# Runs Decryption process

import sys
import twofish as tf
from textwrap import wrap
import time
import psutil
import pickle

start = time.time()
decrypted = []
joined = ""

filepath = sys.argv[1] # get filepath

key = bytes(sys.argv[2], encoding='utf-8') # Generate encryption key
two = tf.Twofish(key)

index = sys.argv[3]
# Get file data into array
with open("E/encrypted_"+index+".txt", "rb") as fp:
    dataList = pickle.load(fp)

for i in range(len(dataList)):
    if len(dataList[i])== 16:
        decrypted.append(two.decrypt(dataList[i]).decode())
    else:
        decrypted.append(dataList[i])

#decrypted_File = open("Decrypted.txt", "w")

for k in range(len(decrypted)):
    joined+= decrypted[k]

#decrypted_File.write(joined)

end = time.time()
duration = end - start
RAM = int(psutil.virtual_memory().total - psutil.virtual_memory().available)

print(joined)


print("\n----Program report----")
print("Execution time: ", round(duration, 2), "seconds")
print("RAM used: ", (RAM/int(psutil.virtual_memory().total)*100), "Mb")




