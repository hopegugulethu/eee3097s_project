# Msimamisi Mwandla
# 30/10/2021

import sys
import twofish as tf
from textwrap import wrap
import time
import psutil
import pickle

start = time.time()
encrypted = []
precomp = []

filepath = sys.argv[1] # get filepath

key = bytes(sys.argv[2], encoding='utf-8') # Generate encryption key
two = tf.Twofish(key)

# Get file data into array
with open(filepath) as file:
    lines= file.readlines()

    for line in lines:
        splitter = wrap(line, 16)
        precomp.append(splitter)

        for i in range(len(splitter)):
            point = splitter[i]
            byter = bytes(point, encoding='utf-8')

            if len(byter) == 16:
                encryptor = two.encrypt(byter)
                encrypted.append(encryptor)
            else:
                encryptor = point
                encrypted.append(encryptor)

end = time.time()
duration = end - start
RAM = int(psutil.virtual_memory().total - psutil.virtual_memory().available)


with open("encrypted.txt", "wb") as fp:
    pickle.dump(encrypted, fp)


print(encrypted)

'''
print("\n----Program report----")
print("Execution time: ", round(duration, 2), "seconds")
print("RAM used: ", (RAM/int(psutil.virtual_memory().total)*100), "%")
'''
