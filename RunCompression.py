# Runs compression algorithm on file data
# Hope Ngwenya
# 30/20/2021

import psutil
import sys
import compress
import time
import pickle

filepath = sys.argv[1] # Get filepath

start = time.time() # start program timer

# Get file data into array
with open(filepath) as file:
    lines= file.readlines()
compressed, tree = compress.compress(lines) # compress the array of lines

stop = time.time() # stop program timer 
duration = stop - start # calculate program duration
RAM = int(psutil.virtual_memory().total - psutil.virtual_memory().available) 

print(compressed)

f = open('treeFile.pkl', 'wb')   # Pickle file created
pickle.dump(tree, f)          # dump tree object into file f
f.close()           

'''
print("\n----Program report----")
print("Execution time: ", round(duration, 2), "seconds")
print("RAM used: ", (RAM/int(psutil.virtual_memory().total)*100), "Mb")
'''
