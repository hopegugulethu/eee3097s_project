# Runs compression algorithm on file data
# Hope Ngwenya
# 30/20/2021
 
import sys
import compress
import time
import psutil
import pickle

filepath = sys.argv[1]
treeFile = sys.argv[2]

start = time.time() # start program timer
stream = ""

# Get file data into array
with open(filepath) as file:
    lines= file.readlines()

    for line in lines:
        stream += line

f = open(treeFile, 'rb')   # 'r' for reading; can be omitted
this_tree = pickle.load(f)         # load file content as mydict
f.close()

print(compress.decompress(stream, this_tree))



