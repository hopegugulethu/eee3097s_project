# runs encryption
import sys
import twofish as tf
import compress
from textwrap import wrap
import time
import os
import psutil
#from memory_profiler import profile

#Mem = psutil.Process(os.getpid())
#prevMem = Mem.memory_info()[0]
#prevSize_Gb = prevMem/2**30
#print("RAM usage before compression:", psutil.virtual_memory()[2])

filepath = sys.argv[1]
key = bytes(sys.argv[2], encoding='utf-8')

two = tf.Twofish(key)

encrypted = []
precomp = []
comp = []
Tree = []

start =time.time()
with open(filepath) as f:
    lines = f.readlines()

    for line in lines:
        splitter = wrap(line, 16)
        precomp.append(splitter)

        for i in range(len(splitter)):
            point = splitter[i]
            byter = bytes(point, encoding='utf-8')

            if len(byter) == 16:
                encryptor = two.encrypt(byter)
                encrypted.append(encryptor)
            else:
                encryptor = point
                encrypted.append(encryptor)

print(encrypted)
#currMem  = Mem.memory_info()[0]
#currSize_Gb = currMem/2**30
#print("RAM usage after encryption:", )
print()
stop = time.time()
t1 =stop-start
start = time.time()
import numpy as np
from itertools import islice

decrypted = []
for j in range(len(encrypted)):
    if len(encrypted[j]) == 16:
        out = two.decrypt(encrypted[j]).decode()
        decrypted.append(out)
    else:
        out = encrypted[j]
        decrypted.append(out)

def blocks(array, sizeOfChunk):
    array = iter(array)
    block = iter(lambda: tuple(islice(array, 8)), ())
    lines = ["".join(c) for c in block]
    return lines

res_array = blocks(decrypted, 8)


#for m in range(len(encrypted)):
#    print(encrypted[m])
print(res_array)
stop =time.time()
t2 =stop -start
print()
print()

start = time.time()
feed = []
for a in range(len(encrypted)):
    if isinstance(encrypted[a], bytes):
        feed.append(encrypted[a])
    else:
        data = bytes(encrypted[a], encoding='utf-8')
        feed.append(data)


c, t =compress.compress(res_array)
stop = time.time()
t3 = stop - start
print(c)
print()
start = time.time()
dec = compress.decompress(c,t)
print(dec)
stop = time.time()
t4 = stop - start

print()
print()
print("Encryption time:", t1, "sec")
print("Decryption time:", t2, "sec")
print("Compression time:", t3, "sec")
print("Decompression time:", t4, "sec")
print("System execution time: ",t1+t2+t3+t4, "sec")
