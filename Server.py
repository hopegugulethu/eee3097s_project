import socket
from _csv import reader

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST = '196.42.112.103'  # server ip
PORT = 1234        # Port to listen on (non-privileged ports are > 1023)
s.bind((HOST, PORT))
s.listen(5)

filepath = "2018-09-19_IMU/2018-09-19-03_57_11_VN100.csv"
forward_array = []  # Array before encryption and compression
reverse_array = []  # Array before decompression and decryption

# Read data from file
with open(filepath, 'r') as read_file:
    csv_file = reader(read_file)
    for line in csv_file:
        forward_array.append(line)

# Clean array - remove 1st and last non-data items
for i in range(len(forward_array)):
    if i == 0:
        forward_array.remove(forward_array[i])
    elif i == len(forward_array) - 1:
        forward_array.remove(forward_array[i])

while True:
    clientSocket, address = s.accept()
    print(f"Connection from {address} has been established")
    for j in range(len(forward_array)):
        clientSocket.send(bytes(str(forward_array[j]), "utf-8"))
    clientSocket.close()
