#!/usr/bin/python3
"""
Program to implement the Huffman Encoding compression algorithm
"""

class TreeNode:
  def __init__(self, freq, sym, left = None, right = None):
    self.freq = freq
    self.sym = sym
    self.right = right
    self.left = left
    self.code = ''


def Frequency(input):
  id  = dict()

  for i in input:
    if id.get(i) == None:
      id[i] = 1
    else:
      id[i] += 1
  return id

code_names = dict()

def Encode(tree_node, value = ''):
  val = value + str(tree_node.code)

  if(tree_node.right):
    Encode(tree_node.right, val)
  if(tree_node.left):
    Encode(tree_node.left, val)
  if(not tree_node.left and not tree_node.right):
    code_names[tree_node.sym] = val
  return code_names


def result(input, coding):
  out = []

  for j in input:
    #print(coding[j], end = '')
    out.append(coding[j])

  s = ''.join([str(k) for k in out])
  file = open("compressed.txt", "a")
  file.write(s)
  return s


def compress(input):
  frequency = Frequency(input)
  id = frequency.keys()
  prob = frequency.values()
  
  #print("symbols: ", id)
  #print("frequencies: ", prob)

  n = []

  for i in id:
    n.append(TreeNode(frequency.get(i), i))

  while len(n) > 1:
    n = sorted(n, key = lambda x:x.freq)
    right = n[0]
    left = n[1]
    left.code = 0
    right.code = 1

    node = TreeNode(left.freq + right.freq, left.sym + right.sym, left, right)
    n.remove(left)
    n.remove(right)
    n.append(node)

  encode = Encode(n[0])
  #print(encode)
  encoded_out = result(input, encode)
  print(Compression_ratio(input, encode))
  return encoded_out, n[0]


def decompress(compressed_data, huffTree):
  htree = huffTree
  decompressed_data = []

  for c in compressed_data:
    if c == '0':
      huffTree = huffTree.left
    elif c == '1':
      huffTree = huffTree.right
    try:
      if huffTree.left.sym == None and huffTree.right.sym == None:
        pass
    except AttributeError:
      decompressed_data.append(huffTree.sym)
      huffTree = htree

  string = ''.join([str(chr) for chr in decompressed_data])
  #file = open("decompressed.txt", "a")
  #file.write(string)
  return string

def Compression_ratio(input, coding):
  before_compression = len(input) * 8 
  after_compression = 0
  symbols = coding.keys()
  for symbol in symbols:
    count = input.count(symbol)
    after_compression += count * len(coding[symbol])
  #print("The compression ratio achieved by this alorithm is: ", after_compression/before_compression)

#input = ["a","a","2","y","s"]
#print(input)
#sol, tree = compress(input)
#print(sol)
#print("Decompressed: ", decompress(sol, tree))
